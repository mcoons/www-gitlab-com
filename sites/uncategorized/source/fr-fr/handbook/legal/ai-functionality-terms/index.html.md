---
layout: localized-handbook-page-toc
title: "AI Functionality Terms"
description: "Learn about GitLab's AI Functionality Terms"
banner_text: "Cette traduction est fournie à titre informatif uniquement. En cas de divergence entre le texte anglais et cette traduction, la version anglaise prévaudra."
---

## Chapitres
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Conditions de fonctionnalité de l'IA

(Conditions supplémentaires)

Les présentes Conditions de fonctionnalité de l'IA (les « Conditions ») sont conclues entre GitLab Inc, dont les bureaux sont situés au 268 Bush Street, Suite 350, San Francisco, CA 94104, États-Unis (ou, toute autre Société affiliée de GitLab répertoriée comme « GitLab » sur un Bon de commande) (« GitLab »), et l'entité acceptant les présentes Conditions (le « Client »). Les présentes Conditions entrent en vigueur à la première des dates suivantes : (a) l'acceptation par le Client des présentes Conditions dans le cadre du (i) Logiciel GitLab ou (ii) via un Bon de commande, ou (b) l'utilisation par le Client de toute Fonctionnalité d'IA (« Date d'entrée en vigueur »).

### 1. DÉFINITIONS

1.1. « Fonctionnalité d'IA » désigne les fonctionnalités du Logiciel GitLab ou les Services supplémentaires achetés séparément, basés sur des technologies d'intelligence artificielle (y compris l'apprentissage automatique).

1.2. « Avenant relatif au traitement des données » désigne l'Avenant relatif au traitement des données de GitLab et les Clauses contractuelles types disponibles à l'adresse <https://about.gitlab.com/handbook/legal/data-processing-agreement/>, ou tout autre avenant écrit relatif au traitement des données entre les parties.

1.3. « Contrat d'abonnement » désigne le contrat d'abonnement GitLab disponible à l'adresse <https://about.gitlab.com/terms/#current-terms-of-use>, ou tout autre accord écrit entre les parties régissant l'utilisation du Logiciel par le Client.

### 2. CHAMP D'UTILISATION

2.1. Les présentes Conditions, ainsi que le Contrat d'abonnement et l'Avenant relatif au traitement des données, régissent l'accès et l'utilisation par le Client de la Fonctionnalité d'IA. Les présentes Conditions sont intégrées au Contrat d'Abonnement et en font partie intégrante.

2.2. Le Client peut, dans le cadre de son utilisation de la Fonctionnalité d'IA, transmettre des Données à caractère personnel à GitLab dans le cadre de l'Entrée (définie ci-dessous). Les obligations du Client en tant que Responsable du traitement en vertu de l'Avenant relatif au traitement des données s'appliqueront dans la mesure où l'utilisation par le Client de la Fonctionnalité d'IA implique le traitement de données à caractère personnel. Tous les sous-traitants utilisés pour fournir la fonctionnalité d'IA seront répertoriés à l'adresse <https://about.gitlab.com/privacy/subprocessors/>, et toutes les notifications de mises à jour de cette liste seront effectuées conformément à la Clause 14 de l'Avenant relatif au traitement des données.

2.3. Le Client peut fournir une entrée à traiter par la Fonctionnalité d'IA (« Entrée »), et recevoir une sortie, y compris du code et du langage naturel, générée et renvoyée par la Fonctionnalité d'IA sur la base de cette Entrée (« Sortie »). Le Client déclare et garantit qu'il a obtenu tous les droits, approbations et consentements nécessaires pour son utilisation de l'Entrée. Aux fins des présentes Conditions, l'Entrée et la Sortie constituent du Contenu Client supplémentaire tel que ce terme est défini dans le Contrat d'Abonnement. Le Client conservera toute propriété du Contenu du Client, comme le permet la loi applicable.

### 3. RESTRICTIONS

3.1 L'utilisation par le Client de la Fonctionnalité d'IA sera conforme aux Conditions d'utilisation acceptables disponibles à l'adresse <https://about.gitlab.com/terms/#current-terms-of-use>.

3.2 Nonobstant toute disposition contraire des présentes Conditions, le Client reconnaît et accepte que (a) la fonctionnalité d'IA peut générer la même sortie, ou une sortie similaire, pour GitLab ou d'autres utilisateurs finaux tiers (par exemple, des clients, des partenaires), et (b) le Client n'a aucun droit, titre ou intérêt à l'égard de GitLab ou des utilisateurs finaux tiers découlant de ou lié à toute sortie générée par la Fonctionnalité d'IA pour GitLab ou des utilisateurs finaux tiers.

3.3 Certaines Fonctionnalités d'IA sont alimentées par des services tiers identifiés et décrits dans la documentation disponible sur docs.gitlab.com (dans chaque cas, un « Fournisseur de services d'IA »). Le Client déclare et garantit qu'il n'utilisera pas la Fonctionnalité d'IA pour créer, former ou améliorer (directement ou indirectement) un modèle de base ou un modèle majeur de langage similaire ou concurrent ou encore un autre service d'intelligence artificielle générative concurrent à celui des Fournisseurs de services d'IA applicables.

3.4 Le Client déclare et garantit qu'il n'effectuera pas d'ingénierie inverse, d'extraction ou de découverte des données, des modèles, des poids de modèle, des algorithmes, des fonctions de sécurité ou du fonctionnement de la Fonctionnalité d'IA.

### 4. CLAUSES DE NON-RESPONSABILITÉ SUPPLÉMENTAIRES

4.1 L'accès et l'utilisation par le Client de la Fonctionnalité d'IA par le Client sont à ses propres risques. La Fonctionnalité d'IA peut générer une Sortie peu fiable, non sécurisée, inexacte ou offensante. Le Client reconnaît et accepte ce qui suit : (i) il évaluera toutes les Sorties avant de s'appuyer sur ces dernières ou de les utiliser autrement ; (ii) il est responsable de s'assurer que toute Sortie incorporée dans sa propriété intellectuelle est conforme aux droits de propriété intellectuelle des tiers ; et (iii) la Fonctionnalité d'IA n'est pas conçue pour ou destinée à être utilisée pour respecter la conformité du Client aux lois ou obligations réglementaires applicables.

4.2 GitLab n'est aucunement responsable de tout acte ou omission de tout Fournisseur de services d'IA ou de la disponibilité, de l'exactitude, des produits ou des services de tout Fournisseur de services d'IA.

4.3 Dans la mesure où la Fonctionnalité d'IA est alimentée ou distribuée par des modèles d'intelligence artificielle open source ou sous licence facultative, ces modèles constituent un Logiciel Community Edition.

### 5. INDEMNISATION ET LIMITATION DE RESPONSABILITÉ

Les dispositions applicables du Contrat d'Abonnement régiront les obligations d'indemnisation et la limitation de responsabilité de chaque partie en ce qui concerne l'utilisation par le Client de la Fonctionnalité d'IA, sachant que GitLab n'assumera aucune responsabilité ou obligation d'indemnisation (envers le Client ou tout tiers, y compris les titulaires de licence de l'Utilisateur ou du Client), à l'égard des réclamations fondées sur une Sortie modifiée.

### 6. DURÉE ET RÉSILIATION
Les présentes Conditions entreront en vigueur à la Date d'entrée en vigueur et expireront automatiquement à (a) l'expiration ou la résiliation du Contrat d'abonnement ou (b) l'expiration ou la résiliation de l'Abonnement sous-jacent à la Fonctionnalité d'IA. Les parties peuvent résilier les présentes Conditions comme prévu dans le Contrat d'Abonnement. Le Client doit, à l'expiration ou à la résiliation des présentes Conditions, immédiatement cesser toute utilisation de la Fonctionnalité d'IA. Les droits et obligations des présentes Conditions survivront, par leur nature, à la résiliation ou à l'expiration de ces présentes dernières.

### 7. DIVERS

Les termes en majuscules utilisés, mais non définis dans les présentes Conditions ont la signification indiquée dans le Contrat d'abonnement ou l'Avenant relatif au traitement des données. Sauf disposition contraire des présentes Conditions, le Contrat d'abonnement reste pleinement en vigueur et régit l'accès et l'utilisation par le Client du Logiciel GitLab. Les présentes Conditions prévaudront en cas de conflit ou d'incohérence entre  lesdites présentes Conditions et le Contrat d'abonnement.