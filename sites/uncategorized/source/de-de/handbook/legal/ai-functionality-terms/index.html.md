---
layout: localized-handbook-page-toc
title: "AI Functionality Terms"
description: "Learn about GitLab's AI Functionality Terms"
banner_text: "Diese Übersetzung dient nur zu Informationszwecken. Im Falle einer Diskrepanz zwischen dem englischen Text und dieser Übersetzung ist die englische Version maßgebend."
---

## Auf dieser Seite
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Begriffe zur KI-Funktionalität

(Zusätzliche Bedingungen)

Diese KI-Funktionalitätsbedingungen („Bedingungen“) gelten zwischen GitLab Inc. mit Sitz in 268 Bush Street, Suite 350, San Francisco, CA 94104 (oder falls ein anderes GitLab-Tochterunternehmen als „GitLab“ auf einem Bestellformular aufgeführt ist) („GitLab“) und dem Unternehmen, das diese Bedingungen akzeptiert („Kunde“). Diese Bedingungen gelten ab dem früheren der beiden folgenden Zeitpunkte: (a) Akzeptieren dieser Bedingungen durch den Kunden innerhalb der (i) GitLab-Software oder (ii) über ein Bestellformular oder (b) Nutzung einer KI-Funktionalität durch den Kunden („Datum des Inkrafttretens“).

### 1. BEGRIFFSDEFINITIONEN

1.1. „KI-Funktionalität“ bezeichnet Funktionen in der GitLab-Software oder in separat erworbenen zusätzlichen Diensten, die auf Technologien der künstlichen Intelligenz (einschließlich maschinellem Lernen) basieren.

1.2. „Datenverarbeitungszusatz“ bezeichnet den GitLab-Datenverarbeitungszusatz und die Standardvertragsklauseln, die unter <https://about.gitlab.com/handbook/legal/data-processing-agreement/> verfügbar sind, oder einen anderen schriftlichen Datenverarbeitungszusatz zwischen den Parteien.

1.3. „Abonnementvertrag“ bezeichnet den GitLab-Abonnementvertrag, der unter <https://about.gitlab.com/terms/#current-terms-of-use> verfügbar ist, oder einen anderen schriftlichen Vertrag zwischen den Parteien, der die Nutzung der Software durch den Kunden regelt.

### 2. UMFANG DER NUTZUNG

2.1. Diese Bedingungen regeln zusammen mit dem Abonnementvertrag und dem Datenverarbeitungszusatz den Zugriff des Kunden auf die KI-Funktionalität und deren Nutzung. Diese Bedingungen sind Bestandteil des Abonnementvertrages und werden in diesen aufgenommen.

2.2. Bei der Nutzung der KI-Funktionalität kann der Kunde personenbezogene Daten als Teil der Eingabe (wie unten definiert) an GitLab übermitteln. Soweit die Nutzung der KI-Funktionalität durch den Kunden die Verarbeitung personenbezogener Daten beinhaltet, gelten die Verpflichtungen des Kunden als für die Verarbeitung Verantwortlicher gemäß dem Zusatz zur Datenverarbeitung. Alle Unterauftragsverarbeiter, die zur Bereitstellung der KI-Funktionalität eingesetzt werden, werden unter <https://about.gitlab.com/privacy/subprocessors/> aufgelistet, und alle Benachrichtigungen über Aktualisierungen dieser Liste erfolgen in Übereinstimmung mit Abschnitt 14 des Zusatzes zur Datenverarbeitung.

2.3. Der Kunde kann Eingaben bereitstellen, die von der KI-Funktionalität verarbeitet werden sollen („Eingaben“), und Ausgaben, einschließlich Code und natürlicher Sprache, erhalten, die von der KI-Funktionalität auf der Grundlage dieser Eingaben erzeugt und zurückgegeben werden („Ausgaben“). Der Kunde sichert zu und gewährleistet, dass er alle erforderlichen Rechte, Genehmigungen und Zustimmungen für die Nutzung der Eingaben erhalten hat. Für die Zwecke dieser Bedingungen stellen sowohl Eingaben als auch Ausgaben zusätzliche Kundeninhalte dar, wie dieser Begriff im Abonnementvertrag definiert ist. Der Kunde behält das gesamte Eigentum an den Kundeninhalten, soweit dies nach geltendem Recht zulässig ist.

### 3. EINSCHRÄNKUNGEN

3.1 Die Nutzung der KI-Funktionalität durch den Kunden erfolgt in Übereinstimmung mit der Richtlinie für die zulässige Verwendung, die unter <https://about.gitlab.com/terms/#current-terms-of-use> verfügbar ist.

3.2 Ungeachtet gegenteiliger Bestimmungen in diesen Bedingungen erkennt der Kunde an und erklärt sich damit einverstanden, dass (a) die KI-Funktionalität dieselbe oder eine ähnliche Ausgabe für GitLab oder andere Endbenutzer von Drittanbietern (z. B. Kunden, Partner) erzeugen kann und (b) der Kunde keinen Anspruch auf Rechte, Titel oder Interessen gegenüber GitLab oder Endbenutzern von Drittanbietern hat, die sich aus der von der KI-Funktionalität für GitLab oder Endbenutzer von Drittanbietern erzeugten Ausgabe ergeben oder damit zusammenhängen.

3.3 Einige KI-Funktionen werden von Diensten Dritter betrieben, die in der unter docs.gitlab.com verfügbaren Dokumentation genannt und beschrieben werden (in jedem Fall ein „KI-Dienstanbieter“). Der Kunde sichert zu und gewährleistet, dass er die KI-Funktionalität nicht verwenden wird, um (direkt oder indirekt) ein ähnliches oder konkurrierendes grundlegendes oder umfangreiches Sprachmodell oder einen anderen generativen Dienst für künstliche Intelligenz anzulegen, zu trainieren oder zu verbessern, der mit den entsprechenden KI-Dienstanbietern im Wettbewerb steht.

3.4 Der Kunde sichert zu und gewährleistet, dass er die Daten, Modelle, Modellanteile, Algorithmen, Sicherheitsmerkmale oder den Betrieb der KI-Funktionalität nicht zurückentwickeln, extrahieren oder nachvollziehen versuchen wird.

### 4. ZUSÄTZLICHE HAFTUNGSAUSSCHLÜSSE

4.1 Der Zugriff auf die KI-Funktionalität und deren Nutzung durch den Kunden erfolgt auf eigenes Risiko. Die KI-Funktionalität kann unzuverlässige, unsichere, ungenaue oder anstößige Ergebnisse erzeugen. Der Kunde erkennt an und erklärt sich damit einverstanden, dass (i) er alle Ausgaben bewertet, bevor er sich auf sie verlässt oder sie anderweitig verwendet; (ii) der Kunde dafür verantwortlich ist, dass alle Ausgaben, die in das geistige Eigentum des Kunden einfließen, mit den Rechten am geistigen Eigentum Dritter übereinstimmen; und (iii) die KI-Funktionalität nicht für die Einhaltung geltender Gesetze oder behördlicher Auflagen durch den Kunden entwickelt wurde oder dafür vorgesehen ist.

4.2 GitLab ist nicht verantwortlich für Handlungen oder Unterlassungen von KI-Dienstanbietern oder für die Verfügbarkeit, Genauigkeit, Produkte oder Dienste von KI-Dienstanbietern.

4.3 Soweit die KI-Funktionalität durch Open-Source- oder freizügig lizenzierte Modelle für künstliche Intelligenz unterstützt oder mit diesen vertrieben wird, stellen diese Modelle Software der Community Edition dar.

### 5. ENTSCHÄDIGUNG; HAFTUNGSBESCHRÄNKUNG

Die geltenden Bestimmungen des Abonnementvertrags regeln die Entschädigungsverpflichtungen und die Haftungsbeschränkung jeder Partei in Bezug auf die Nutzung der KI-Funktionalität durch den Kunden, mit der Ausnahme, dass GitLab weder eine Haftung noch eine Entschädigungsverpflichtung (gegenüber dem Kunden oder Dritten, einschließlich der Lizenznehmer des Benutzers oder des Kunden) in Bezug auf Ansprüche hat, die auf modifizierten Ausgaben basieren.

### 6. LAUFZEIT UND KÜNDIGUNG
Diese Bedingungen treten mit dem Datum des Inkrafttretens in Kraft und enden automatisch mit (a) dem Ablauf oder der Beendigung des Abonnementvertrags oder (b) dem Ablauf oder der Beendigung des zugrunde liegenden Abonnements für die KI-Funktionalität. Die Parteien können diese Bedingungen gemäß den Bestimmungen des Abonnementvertrags kündigen. Nach Ablauf oder Beendigung dieser Bedingungen hat der Kunde die Nutzung der KI-Funktionalität unverzüglich einzustellen. Die Rechte und Pflichten dieser Bedingungen, die ihrer Natur nach dazu bestimmt sind, die Kündigung oder den Ablauf dieser Bedingungen zu überdauern, bleiben bestehen.

### 7. SONSTIGES

Begriffe in Großbuchstaben, die in diesen Bedingungen verwendet, aber nicht definiert werden, haben die Bedeutung, die im Abonnementvertrag oder im Zusatz zur Datenverarbeitung angegeben ist. Sofern in diesen Bedingungen nichts anderes vorgesehen ist, bleibt der Abonnementvertrag in vollem Umfang in Kraft und regelt den Zugriff des Kunden auf die Software von GitLab und deren Nutzung. Im Falle von Konflikten oder Unstimmigkeiten zwischen diesen Bedingungen und dem Abonnementvertrag haben die vorliegenden Bedingungen Vorrang.